module.exports = {
	prerun: function(creep) {
		if (creep.carry.energy == 0) {
			creep.memory.task = "collectEnergy";
			delete creep.memory.target;
		} else if (creep.carry.energy >= creep.carryCapacity / 2) {
			creep.memory.task = "refill";
			delete creep.memory.target;
		}

		creep.target = Game.getObjectById(creep.memory.target);

		if (creep.memory.task == "collectEnergy") {
			var closestDroppedEnergy = creep.pos.findClosestByRange(FIND_DROPPED_RESOURCES, {
				filter: o => o.resourceType == RESOURCE_ENERGY
			});

			if (!creep.target || creep.pos.getRangeTo(creep.target) > creep.pos.getRangeTo(closestDroppedEnergy)) {
				creep.target = closestDroppedEnergy;

				var closestContainer = creep.pos.findClosestByRange(FIND_STRUCTURES, {
					filter: o => o.structureType == STRUCTURE_CONTAINER &&
							o.store.energy > creep.carryCapacity
				});

				if (!creep.target || creep.pos.getRangeTo(creep.target) > creep.pos.getRangeTo(closestContainer)) {
					creep.target = closestContainer;
				}
			}

			if (!creep.target && creep.room.storage && creep.room.storage.store.energy > creep.carryCapacity) {
				creep.target = creep.room.storage;
			}
		}

		if (creep.memory.task == "refill") {
			if (!creep.target) {
				creep.target = creep.pos.findClosestByPath(FIND_STRUCTURES, {
					filter: o => o.structureType == STRUCTURE_TOWER && o.energy < o.energyCapacity * 0.1
				});
			}

			if (!creep.target) {
				creep.target = creep.pos.findClosestByPath(FIND_MY_CREEPS, {
					filter: o => {
						if (!o.workers) o.workers = 0;
						return o.memory.role == "builder" && o.carry.energy < o.carryCapacity && o.workers == 0;
					}
				});
			}

			if (!creep.target || (creep.target instanceof Structure && creep.target.energy == creep.target.energyCapacity)) {
				creep.target = creep.pos.findClosestByPath(FIND_STRUCTURES, {
					filter: o => {
						if (!o.workers) o.workers = 0;
						return (o.structureType == STRUCTURE_SPAWN || o.structureType == STRUCTURE_EXTENSION) && o.energy < o.energyCapacity &&
						(o.energyCapacity - o.energy < creep.carry.energy || creep.carryCapacity <= o.energyCapacity) && o.workers == 0;
					}
				});
			}

			if (!creep.target || (creep.target instanceof Creep && creep.target.carry.energy > creep.target.carryCapacity)) {
				creep.target = creep.pos.findClosestByPath(FIND_MY_CREEPS, {
					filter: o => {
						if (!o.workers) o.workers = 0;
						return (o.memory.role == "builder" || o.memory.role == "upgrader") && o.carry.energy < o.carryCapacity &&
						(o.carryCapacity - o.carry.energy < creep.carry.energy || creep.carryCapacity <= o.carryCapacity) && o.workers == 0;
					}
				});
			}
			
			if (!creep.target) {
				creep.target = creep.pos.findClosestByPath(FIND_STRUCTURES, {
					filter: o => o.structureType == STRUCTURE_TOWER && o.energy < o.energyCapacity
				});
			}
		}

		if (creep.target) {
			creep.target.workers += 1;
			creep.memory.target = creep.target.id;
		} else {
			if (creep.carry.energy < creep.carryCapacity * 0.5) {
				creep.memory.task = "collectEnergy";
				delete creep.memory.target;
			} else {
				creep.memory.task = "refill";
				delete creep.memory.target;
			}
		}
	},

	run: function(creep) {
		if (creep.memory.task == "collectEnergy") {
			if (creep.target instanceof Structure) {
				if (creep.withdraw(creep.target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
					creep.moveTo(creep.target);
				}
			}

			if (creep.target instanceof Resource) {
				if (creep.pickup(creep.target) == ERR_NOT_IN_RANGE) {
					creep.moveTo(creep.target);
				}
			}
		}

		if (creep.memory.task == "refill") {
			if (creep.target instanceof Structure || creep.target instanceof Creep) {
				if (creep.transfer(creep.target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
					creep.moveTo(creep.target);
				}
			}
		}
	}
};
