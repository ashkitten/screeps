module.exports = {
	prerun: function(creep) {
		creep.target = creep.pos.findClosestByRange(FIND_CONSTRUCTION_SITES);
	},

	run: function(creep) {
		if (creep.target) {
			if (creep.build(creep.target) == ERR_NOT_IN_RANGE) {
				creep.moveTo(creep.target);
			}
		} else {
			// Be an upgrader if there's nothing else to do
			require("role.upgrader").run(creep);
		}
	}
};
