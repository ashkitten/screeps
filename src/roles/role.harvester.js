module.exports = {
	prerun: function(creep) {
		creep.target = Game.getObjectById(creep.memory.target);

		if(creep.target) {
			if(!creep.target.workers) creep.target.workers = 0;
			creep.target.workers += 1;
		}
	},

	run: function(creep) {
		if (creep.target) {
			// Find closest non-full container in range
			var containers = creep.pos.findInRange(FIND_STRUCTURES, 1, {
				filter: o => o.structureType == STRUCTURE_CONTAINER || o.structureType == STRUCTURE_STORAGE
			});
			var nonFullContainer = _.filter(containers, o => _.sum(o.store) < o.storeCapacity)[0];

			if (creep.harvest(creep.target) == ERR_NOT_IN_RANGE) { // If creep is out of range
				// Move to source
				creep.moveTo(creep.target);
			} else {
				// Transfer energy to non-full container
				if (nonFullContainer) {
					creep.transfer(nonFullContainer, RESOURCE_ENERGY)
				} else {
					if (!creep.pos.findInRange(FIND_CONSTRUCTION_SITES, 1, containers.length) &&
						!creep.pos.findInRange(FIND_STRUCTURES, 1, { filter: o => o.structureType == STRUCTURE_CONTAINER || o.structureType == STRUCTURE_STORAGE }).length) {
						creep.pos.createConstructionSite(STRUCTURE_CONTAINER);
					}
				}
			}
		}
	}
};
