module.exports = {	
	prerun: function(creep) {
		require("role." + creep.memory.role).prerun(creep);
	},

	run: function(creep) {
		var nearbyDrops = creep.pos.findInRange(FIND_DROPPED_RESOURCES, 1, { filter: { resourceType: RESOURCE_ENERGY } });
		var nearbyContainers = creep.pos.findInRange(FIND_STRUCTURES, 1, { filter: { structureType: STRUCTURE_CONTAINER } });

		nearbyDrops.forEach(o => creep.pickup(o));
		nearbyContainers.forEach(o => creep.withdraw(o, RESOURCE_ENERGY));

		require("role." + creep.memory.role).run(creep);
	}
}
