module.exports = {
	prerun: function(creep) {},

	run: function(creep) {
		if (creep.pos.inRangeTo(creep.room.controller, 3)) {
			creep.upgradeController(creep.room.controller);
		} else {
			creep.moveTo(creep.room.controller);
		}
	}
};
