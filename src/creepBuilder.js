/*
 * TODO:
 */

var CREEP_TYPES = {
	builder: {
		base: [WORK, CARRY, MOVE],
		add: [WORK, CARRY, MOVE],
		maxAdd: Infinity
	},
	harvester: {
		base: [WORK, CARRY, MOVE],
		add: [WORK, WORK],
		maxAdd: 2
	},
	refiller: {
		base: [CARRY, MOVE],
		add: [CARRY, CARRY, MOVE],
		maxAdd: Infinity
	},
	upgrader: {
		base: [WORK, CARRY, MOVE],
		add: [WORK, WORK, CARRY, MOVE],
		maxAdd: Infinity
	}
}

module.exports = {
	/**
	 * @param {Spawn} spawn
	 * @param {string} creepType
	 */
	buildCreep: function(spawn, creepType) {
		var creepModules = CREEP_TYPES[creepType].base;

		if (Cache.refillers.length) {
			var numAdd = Math.floor((spawn.room.energyCapacityAvailable - _.sum(creepModules, p => BODYPART_COST[p])) /
				_.sum(CREEP_TYPES[creepType].add, p => BODYPART_COST[p]));

			if (numAdd > CREEP_TYPES[creepType].maxAdd) numAdd = CREEP_TYPES[creepType].maxAdd;

			for (var i = 0; i < numAdd; i++) {
				creepModules = creepModules.concat(CREEP_TYPES[creepType].add);
			}
		}

		var target;
		if (creepType == "harvester") {
			var sources = spawn.room.find(FIND_SOURCES);
			target = spawn.pos.findClosestByRange(sources, {
				filter: o => {
					if (!o.workers) o.workers = 0;
					if (!Cache.harvesters.length) return true;
					return o.workers < Cache.harvesters.length / sources.length;
				}
			}).id;
		}

		var result = spawn.createCreep(creepModules, undefined, { role: creepType, target: target });
		if (typeof result == "string") {
			console.log("Spawning new " + creepType + ": " + result);
		}
	}
}
