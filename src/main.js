/**
 * TODO:
 * Multi-room mining
 * Assign jobs to creeps
 * Comment everything
 */

var util = require("util");
var creepBuilder = require("creepBuilder");

const profiler = require('screeps-profiler');
if (false) { // use this to toggle the profiler on or off at compile time
	profiler.enable();
}

module.exports = {
	loop: function() {
		profiler.wrap(this.run.bind(this));
	},

	run: function() {
		if(Game.time % 100 == 0) {
			util.cleanMemory();
		}

		global.Cache = {};

		Cache.builders = _.filter(Game.creeps, o => o.memory.role == "builder");
		Cache.upgraders = _.filter(Game.creeps, o => o.memory.role == "upgrader");
		Cache.harvesters = _.filter(Game.creeps, o => o.memory.role == "harvester");
		Cache.refillers = _.filter(Game.creeps, o => o.memory.role == "refiller");

		Cache.towers = _.filter(Game.structures, structure => structure.structureType == STRUCTURE_TOWER);

		Cache.spawns = {};

		for (var name in Game.creeps) { require("role.base").prerun(Game.creeps[name]); }
		for (var name in Game.creeps) { require("role.base").run(Game.creeps[name]); }

		// reverse order
		for (var name in Game.spawns) {
			var spawn = Game.spawns[name];
			spawn.Cache = {};

			var sources = spawn.room.find(FIND_SOURCES_ACTIVE);
			var constructionSites = spawn.room.find(FIND_CONSTRUCTION_SITES);

			var neededHarvesters = Math.ceil(sources.length * 5 / (spawn.room.energyCapacityAvailable / 200));
			if (neededHarvesters < sources.length) neededHarvesters = sources.length;
			if (neededHarvesters > sources.length * 3) sources.length * 3;
			var neededRefillers = Cache.harvesters.length * 1;

			spawn.Cache.neededHarvesters = neededHarvesters;
			spawn.Cache.neededRefillers = neededRefillers;

			if (Cache.harvesters.length < neededHarvesters) creepBuilder.buildCreep(spawn, "harvester");
			if (Cache.builders.length < 1 && constructionSites.length && Cache.harvesters.length == neededHarvesters) creepBuilder.buildCreep(spawn, "builder");
			if (Cache.upgraders.length < 1 && Cache.harvesters.length == neededHarvesters) creepBuilder.buildCreep(spawn, "upgrader");
			if (Cache.refillers.length < neededRefillers && Cache.harvesters.length) creepBuilder.buildCreep(spawn, "refiller");
		}

		for (var tower of Cache.towers) {
			var closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);

			var closestDamagedCreep = tower.pos.findClosestByRange(FIND_MY_CREEPS, {
				filter: o => o.hits < o.hitsMax
			});

			var closestDamagedStructure = tower.pos.findClosestByRange(FIND_STRUCTURES, {
				filter: o => (o.structureType == STRUCTURE_CONTAINER || o.structureType == STRUCTURE_ROAD) &&
					o.hits < o.hitsMax
			});

			var walls = tower.room.find(FIND_STRUCTURES, {
				filter: o => o.structureType == STRUCTURE_WALL && o.hits < 2000000
			});
			var mostDamagedWall = null;
			if(walls.length) mostDamagedWall = _.min(walls, o => o.hits);

			if (mostDamagedWall && !closestDamagedCreep && !closestDamagedStructure && !closestHostile && tower.energy > tower.energyCapacity * 0.5) {
				var emptyExtensions = tower.room.find(FIND_STRUCTURES, {
					filter: o => o.structureType == STRUCTURE_EXTENSION && o.energy == 0
				});
				if (!emptyExtensions.length) tower.repair(mostDamagedWall);
			}

			if (closestDamagedCreep && !closestHostile && tower.energy > tower.energyCapacity * 0.2) {
				tower.heal(closestDamagedCreep);
			}

			if (closestDamagedStructure && !closestDamagedCreep && !closestHostile && tower.energy > tower.energyCapacity * 0.2) {
				tower.repair(closestDamagedStructure);
			}

			if (closestHostile) {
				tower.attack(closestHostile);
			}
		}
	}
}
